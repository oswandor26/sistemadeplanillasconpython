from time import sleep
from rich import print
from rich.prompt import Console  , Prompt
from rich.progress import track
from rich.table import Table
import pandas as pd
 
ISS = 0.03
AFP  = 0.0725

#funcion principla para obtener los empleados 
def CrearEmpleados():

    # se crean las varibles tipo globales para poder acceder en otras funciones   
    global lista_de_empleados
    global cantidadEmpleadosAgregar

    lista_de_empleados = {
        "codigo":[], 
        "nombre" :[],
        "Sueldoempelado":[], 
        "ISS": [], 
        "AFP": [], 
        "SAR": [], 
        "RENTA":[], 
        "sueldomesualfinal":[]
    }
    try:

        cantidadEmpleadosAgregar   =  Prompt.ask(  ":atom_symbol: Ingrese el numero de empleados [b]1[/b] a [b]n...[/b]", default=1)
        
        #barra de carga 
        for n in track(range(int(cantidadEmpleadosAgregar))  , "Procesando..." ): 
            sleep(0.4)

        # añadir datos de empleados
        contadordefault = 1  
        for i in range(int(cantidadEmpleadosAgregar)): 

            codigo = Prompt.ask(":green_book: Ingrese su codigo de Empleado" , default=str(contadordefault)) 
            sueldo = Prompt.ask("Ingrese su sueldo Mensual" ,default="300" )
            nombre = Prompt.ask("Ingrese su Nombre completo" ,  default="Paul Atreides" )

            #convertir a float 
            sueldo = float(sueldo)
    
            # se calculan los descuentos ISSS y AFP 
            descuentoISSS = sueldo  * ISS
            descuentoAFP = sueldo * AFP 

            sueldoantedelarenta = sueldo - (descuentoISSS + descuentoAFP)

            #se calcula la renta 
            if sueldo > 1  and  sueldo <= 500:
                renta = 0 
            elif sueldo  > 500 and sueldo <= 1000:
                renta = 0.10
            elif sueldo > 1000: 
                renta = 0.20 

            descuendorenta = sueldoantedelarenta * renta 

            lista_de_empleados["codigo"].append(codigo) 
            lista_de_empleados["nombre"].append(nombre)
            lista_de_empleados["Sueldoempelado"].append(format(sueldo , '.2f')) 
            lista_de_empleados["ISS"].append(format(descuentoISSS , '.2f'))
            lista_de_empleados["AFP"].append(format(descuentoAFP  , '.2f')) 
            lista_de_empleados["SAR"].append(format(sueldoantedelarenta , '.2f'))  
            lista_de_empleados["RENTA"].append(format(descuendorenta , '.2f'))   
            lista_de_empleados["sueldomesualfinal"].append(format((sueldoantedelarenta - descuendorenta) , '.2f')) 

            contadordefault+=1 
      

    except Exception as e : 
        console.print("\n [bold red] Error Tipo de dato {} intenta de nuevo...".format(type(e)))
        CrearEmpleados() 



#para volver a calcular  el sueldo 
def hacercalculo(sueldo:float , idmodificar:int)->None:

        # se calculan los descuentos ISSS y AFP 
        descuentoISSS = sueldo  * ISS
        descuentoAFP = sueldo * AFP 

        sueldoantedelarenta = sueldo - (descuentoISSS + descuentoAFP)

        #se calcula la renta 
        if sueldo > 1  and  sueldo <= 500:
            renta = 0 
        elif sueldo  > 500 and sueldo <= 1000:
            renta = 0.10
        elif sueldo > 1000: 
            renta = 0.20 

        descuendorenta = sueldoantedelarenta * renta 

        lista_de_empleados["Sueldoempelado"][idmodificar] = format(sueldo , '.2f')  
        lista_de_empleados["ISS"][idmodificar] = format(descuentoISSS , '.2f') 
        lista_de_empleados["AFP"][idmodificar] = format(descuentoAFP , '.2f')
        lista_de_empleados["SAR"][idmodificar] = format(sueldoantedelarenta , '.2f') 
        lista_de_empleados["RENTA"][idmodificar] = format(descuendorenta , '.2f' ) 
        lista_de_empleados["sueldomesualfinal"][idmodificar] = format((sueldoantedelarenta - descuendorenta) , '.2f')  




#para modificar los empleados 
def modificardatosdeempleado(idnumber:int):

    # el numero  de indice del array 
    numeroid=lista_de_empleados["codigo"].index(str(idnumber)) 
    
    try: 

        quequieremodificar = Prompt.ask("Que desea modificar?", choices=["nombre", "sueldo"], default="nombre")

        if quequieremodificar == "nombre":
            
            nombrenuevo = Prompt.ask("Ingrese el nuevo nombre: ")
            lista_de_empleados["nombre"][numeroid] = str(nombrenuevo)
        

        elif quequieremodificar == "sueldo": 
            nuevosueldo = Prompt.ask("Ingrese el nuevo sueldo: ")
            hacercalculo(float(nuevosueldo) ,numeroid) 
       

    except Exception as e : 

        console.print("\n [bold red] Error Tipo de dato {} intenta de nuevo...".format(type(e)))
        modificardatosdeempleado(idnumber)
  



#para mostrar la tabla 
def mostrarempleados():

    print("\n")     
    table = Table(title="Planilla de Empleados")

    table.add_column("Codigo", justify="right", style="cyan", no_wrap=True)
    table.add_column("Nombre", style="magenta")
    table.add_column("Sueldo", justify="right", style="green")
    table.add_column("ISSS", justify="right", style="cyan", no_wrap=True)
    table.add_column("AFP", style="magenta")
    table.add_column("SAR", justify="right", style="green")
    table.add_column("RENTA", justify="right", style="cyan", no_wrap=True)
    table.add_column("Sueldo Final", style="magenta")
     
    #se obtiene un diccioanrio de forma que en ese se almacena de forma paralela 
    #{'codigo': ['1', '2'], 'nombre': ['Paul Atreides', 'Juan'], 'Sueldoempelado': ['300.00', '400.00'], 'ISS': ['9.00', '12.00'], 'AFP': ['21.75', '29.00'], 'SAR': ['269.25', '359.00'], 'RENTA': ['0.00', '0.00'], 'sueldomesualfinal': ['269.25', '359.00']}
    
    # listadeparatalbas se itera y se crea un array de 2 dimensiones
    #[['1', '2'], ['Paul Atreides', 'Juan'], ['300.00', '400.00'], ['9.00', '12.00'], ['21.75', '29.00'], ['269.25', '359.00']] 
    listadeparatablas = []
    for key , valor in lista_de_empleados.items():
        listadeparatablas.append(valor) 

    # se ordena el array por filas   y colmunas   
    daframe = pd.DataFrame(listadeparatablas , columns=[str(x)  for x in range(0,int(cantidadEmpleadosAgregar))])
    #se crea nueva lista para separa los en  de forma ordenada como se ingresaron los datos 
    #[array(['1', 'Paul At...pe=object), array(['2', 'Juan', ...pe=object)]
    nuevalista = []
    for column in daframe:
        nuevalista.append(daframe[column].values) 

    for i in range(len(nuevalista)):
        table.add_row(
            str(nuevalista[i][0]),
            str(nuevalista[i][1]),
            str(nuevalista[i][2]),
            str(nuevalista[i][3]),
            str(nuevalista[i][4]),
            str(nuevalista[i][5]),
            str(nuevalista[i][6]),
            str(nuevalista[i][7]))

    console = Console()
    console.print(table)

def generarArchivoDePlanillas() :

    try: 
      
        #[['1', '2'], ['Paul Atreides', 'Juan'], ['300.00', '400.00'], ['9.00', '12.00'], ['21.75', '29.00'], ['269.25', '359.00']] 
        listadeparatablas = []
        for key , valor in lista_de_empleados.items():
            listadeparatablas.append(valor) 

        # se ordena el array por filas   y colmunas   
        daframe = pd.DataFrame(listadeparatablas , columns=[str(x)  for x in range(0,int(cantidadEmpleadosAgregar))])
        #se crea nueva lista para separa los en  de forma ordenada como se ingresaron los datos 
        #[array(['1', 'Paul At...pe=object), array(['2', 'Juan', ...pe=object)]
        with open("planilla.txt" , "a") as f :
            f.write("""Codigo ┃ Nombre        ┃ Sueldo ┃ ISSS ┃ AFP   ┃    SAR ┃ RENTA ┃ Sueldo Final \n""");  
        nuevalista = []

        for column in daframe:
            nuevalista.append(daframe[column].values) 
            with open("planilla.txt" , "a" ) as f: 
                for i in range(len(nuevalista)):
                    strilista = str(nuevalista[i][0]+"\t"*2+
                    nuevalista[i][1] +"\t"+
                    nuevalista[i][2]+"\t"+
                    nuevalista[i][3]+"\t"+
                    nuevalista[i][4]+"\t"+
                    nuevalista[i][5]+"\t"*2+
                    nuevalista[i][6]+"\t"*2+
                    nuevalista[i][7]+"\n"); 
                    f.write(strilista); 
    except NameError as e:
        print("no existe ningun registro de empleados  vuelve a intentar añadiendo registros")
        CrearEmpleados() 
    except Exception as e:
        print(e)   



    

if __name__ == "__main__":

    try:
        from rich.style import Style
        prompt = Prompt()
        console = Console()
      

        style = Style(color="yellow",bold=True )
        console.rule("Sistema de Planillas")
        console.print(":warning:")
        console.print("""

                                add_employees:          añadir Empleados.
                                show_employees:         Mostrar Empleados.
                                modify_employee:        Modificar Empleados.
                                Generate_file           Generar archivo de planilla.
                                quit:                   salir de programa.
                                help:                   muestra ayuda. 
                        """ , style=style)

        while True:
            choice = prompt.ask("[bold red] Que te gustaria hacer?" )

            if choice == "quit":
               break
            elif choice == "add_employees":
                CrearEmpleados()
                mostrarempleados()
            elif choice == "show_employees":
                mostrarempleados()
            elif choice == "modify_employee":
                id = int(input("ingresar el id:")) 
                if isinstance(id , int):
                    modificardatosdeempleado(id)  
                    mostrarempleados()
                else: 
                    print()
            elif choice == "help":
                print("""
                add_employees: Add Employees
                show_employees: Show Employees
                modify_employee: Modify Employee
                delete_employee: Delete Employee
                Generate_file:  Generar archivo de planilla
                quit: Quit
                help: Help
                """)
            elif choice == "Generate_file":
                generarArchivoDePlanillas() 
            else:
                console = Console()
                console.print("\n [yellow underline]Esta acción no se encuentra vuelve a intentar de nuevo.\n")

    except Exception as e:

        print(e)